# SSH Tunnel App for Open OnDemand

This is an interactive App for [Open OnDemand](https://openondemand.org/). It starts a job on the cluster and then runs an sshd server with user priviliges there.

You can then direct Visual Studio Code to that server and run its server inside that job.

It supports direct connections as well as apptainer containers. X11 forwarding works as well.

This has been heavily inspired by https://crc-pages.pitt.edu/user-manual/slurm/vscode/

## How to use this as a user
When this app has been added to Open OnDemand by your admins, you should see it in your interactive apps drawer. In order to use it, you need to do a one time setup on your computer and the cluster as well.

I assume that your cluster has at least one login node. This is where you normally ssh to submit jobs. Whenever you `YOUR_LOGIN_NODE` in the instructions, put that hostname in there.

And you also have a username on the cluster. This is what you insert when you see `YOUR_USER_NAME`.

### On your local computer

#### Only if you use MS Windows
The SSH Version that comes with Windows **is not capable of doing this!!** You therefore need to install a different version. IMHO, installing git is the best way to go because it:

1. contains a correct SSH version
2. is easy to install
3. you should have git installed anyhow

To do this, the easiest way is to open a Powershell by right-click on the Start Menu and then click `Windows PowerShell`. In the window that opens, execute this:

```
winget install git.git
```

After the installation has finished, you are going to find a new entry called `Git Bash`. Close the PowerShell window and open the `Git Bash` terminal. Now you can follow the rest of the instructions.

**Please see below for extra instructions in Visual Studio Code because otherwise it is not going to work!!!**

#### If you use Linux or Mac or have completed the above extra steps

If you have not done this, you need to create a private ssh key for passwordless connection to your cluster. You can use the default one (normally called `id_rsa`) but I like keeping them separate.

For this example, we are going to create a new one and save it to the file `~/.ssh/cluster`

`ssh-keygen -t rsa -f ~/.ssh/cluster`

Now, we need to tell the cluster that the key is yours:

`ssh-copy-id -i ~/.ssh/cluster YOUR_USER_NAME@YOUR_LOGIN_NODE`

Now, open `~/.ssh/config` (for example using `nano ~/.ssh/config` and add the following lines (and don't forget to change `YOUR_LOGIN_NODE` and `YOUR_USERNAME`):

```
Host tunnel_target  
  ControlMaster auto  
  ControlPath ~/.ssh/master-%r@%h:%p  
  HostName YOUR_LOGIN_NODE
  User YOUR_USER_NAME
  IdentityFile ~/.ssh/cluster
  ForwardAgent yes
  ForwardX11 yes
  ForwardX11Trusted yes

Host tunnel_portal  
  ProxyCommand ssh -Y tunnel_target "nc \$(squeue --me --name=tunnel --states=R -h -O NodeList:50,Comment)"  
  StrictHostKeyChecking no  
  User YOUR_USER_NAME
  IdentityFile ~/.ssh/cluster
  ForwardAgent yes 
  ForwardX11 yes 
  ForwardX11Trusted yes
```

### Preparations on the cluster
Now, login to the login node and create a custom ssh host key. As we are going to run sshd without root priviliges, it needs this.

1. Log in to some cluster node.
2. `ssh-keygen -t rsa -f ~/.ssh/user_ssh -N ''`

### Try it!
Now, you are all set. Go to your Open OnDemand, start the Tunnel App.

**Make sure that you do not choose an Apptainer Image for the first time**

As soon as it is running, go to your local computer and write:

`ssh tunnel_portal`

This should bring you directly into the job on a compute node.

### Connect VS Code
Launch VS Code.

If you have not done so, install the `Remote - SSH` Extension.

#### If you use Windows
We need to tell VS Code to use the correct SSH version:

1. Press `Ctrl+,` or click `File->Preferences->Settings`.
2. Search for `remote.ssh: Path`
3. Enter: `C:/Program Files/Git/usr/bin/ssh.exe`

#### If you use Linux or Mac or have completed the above extra steps

Now, press `Ctrl+Shift+P` to open the command palette, choose `Remote-SSH: Connect to Host...` and just enter `tunnel_portal` as the host.

## For admins: How to install it
Take a look [here](https://osc.github.io/ood-documentation/latest/install-ihpc-apps.html)